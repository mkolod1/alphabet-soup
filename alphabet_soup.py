'''
File: alphabet_soup.py
Author: Matthew Kolodner
Last Modified: 3/8/2021
Description: This file contains the code for providing an answer key to a word search puzzle from the Sunday print. The
answer key displays each word that has been chosen as well as the starting and ending indices of each word.
Package Manager: PIP
Worst-Case Runtime: O(RCNW)
R: # of rows
C: # of cols
N: Number of hidden words
W: Longest length word among hidden words
'''


import sys


class Alphabet_Soup:
    def __init__(self, dim, grid, words_to_find, bool_grid):
        self.dim = dim
        self.grid = grid
        self.words_to_find = words_to_find
        self.bool_grid = bool_grid

    '''
    Function: valid_neighbors:
    Determines whether a specified neighbor is a valid neighbor, meaning it is in bounds and has not already been visited
    @param row: Candidate row in grid
    @param col: Candidate col in grid
    @return: Whether or not the specified neighbor is a valid neighbor
    '''
    def valid_neighbors(self, row, col):
        if row < 0 or row >= self.dim[0]:  # Checks if row is in bounds
            return False
        if col < 0 or col >= self.dim[1]:  # Checks if col is in bounds
            return False
        if self.bool_grid[row][col]:  # Checks if the candidate row and col have been visited
            return False
        return True


    '''
    Function: find_end
    Recursive function to find the end indices of a given word in a word puzzle grid of characters
    @param word: Candidate word to be found
    @param row: Candidate row in grid
    @param col: Candidate col in grid
    @return: List of indices of final letter in word. This list is empty if the word is not found.
    '''

    def find_end(self, word, row, col):
        # Base Case 1: Word length is 1, so we want to check if we have completed word and return row and col
        if len(word) == 1 and word[0] == self.grid[row][col].upper():
            return [row, col]
        # Base Case 2:First letter of word doesn't match up, so we return an empty list
        if word[0] != self.grid[row][col].upper():
            return []
        self.bool_grid[row][col] = True  # Marks current location as visited
        for dr in reversed(range(-1, 2)):
            for dc in reversed(range(-1, 2)):
                if dr == 0 and dc == 0:  # Iterates through each of the 8 neighboring locations, excluding the current location
                    continue
                newr = row + dr
                newc = col + dc
                if self.valid_neighbors(newr, newc):  # Recursive case looks through all valid neighbors
                    found = self.find_end(word[1:], newr, newc)
                    if found:
                        self.bool_grid[row][col] = False  # Unmarks all locations once word has been found
                        return found
                self.bool_grid[row][col] = False  # Unmarks current location as visited
        return []


    '''
    Function: find_word
    Finds the candidate start/end indices of the specified word and prints them
    @param word: word to be located within the grid
    '''
    def find_word(self, word):
        for i in range(self.dim[0]):
            for j in range(self.dim[1]):
                if word[0].upper() == self.grid[i][j].upper():
                    found = self.find_end(word.replace(" ", "").upper(), i, j)
                    if found:
                        print(word + ' ' + str(i) + ':' + str(j) + ' ' + str(found[0]) + ':' + str(found[1]))
                        return
    '''
    Function: find_words
    Finds and prints all the hidden words in words_to_find within the specified grid and their start/end locations
    '''
    def find_words(self):
        for word in self.words_to_find:
            if word == '':
                continue
            self.find_word(word)


'''
Function: read_input:
Reads and deconstructs the input from the file into the dimension, grid, and list of words to find
@param lines: String list of lines from the input file
@return dim: Tuple containing dimensions of the input grid
@return grid: Word puzzle grid of characters in the format of a list of list of strings
@return words_to_find: List of hidden words to find from input grid
'''
def read_input(lines):
    first = lines[0].strip()
    dim = tuple([int(x) for x in first.split('x')])  # Extracting dim
    grid = []
    words_to_find = []
    for line in lines[1:1 + dim[0]]:  # Extracting grid
        curline = line.strip()
        grid.append(curline.split())
    for line in lines[1 + dim[0]:]:  # Extracting hidden words
        words_to_find.append(line.strip())
    return dim, grid, words_to_find


'''
Function: do_bool_grid:
Constructs a grid of booleans, initialized to false, with the same dimensions as dim
@param dim: Input dimensions of grid
@return bool_grid: grid of booleans, initialized to false, in the format of a list of list of strings
'''
def do_bool_grid(dim):
    bool_grid = []
    for i in range(dim[0]):
        cur = []
        for j in range(dim[1]):
            cur.append(False)
        bool_grid.append(cur)
    return bool_grid



'''
Function: alphabet_soup
Loads a text file containing an input dimension size, grid, and list of words to find. Prints out each hidden word and
their start and end indices
@param filename: Source file name containing input dimension, grid, and words to find information
'''

def do_alphabet_soup(filename):
    with open(filename) as f:
        lines = f.readlines()
        dim, grid, words_to_find = read_input(lines)
        bool_grid = do_bool_grid(dim)
        model = Alphabet_Soup(dim, grid, words_to_find, bool_grid)
        model.find_words()

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Please enter one filename")
    else:
        do_alphabet_soup(sys.argv[1])
